# php-extended/php-enumerable
A simple library to provide access to enumerable functionnalities in php

![coverage](https://gitlab.com/php-extended/php-enumerable/badges/master/pipeline.svg?style=flat-square)
![build status](https://gitlab.com/php-extended/php-enumerable/badges/master/coverage.svg?style=flat-square)


## Installation

The installation of this library is made via composer and the autoloading of
all classes of this library is made through their autoloader.

- Download `composer.phar` from [their website](https://getcomposer.org/download/).
- Then run the following command to install this library as dependency :
- `php composer.phar php-extended/php-enumerable ^7`


## Basic Usage

The basic usage of this library is as follows. This is an example implementation
given a language choice to be into a closed list.

```php

class Language extends \Enumerable
{
	
	/**
	 * The German language.
	 * 
	 * @return \Language
	 */
	public static function DE() : \Language
	{
		$l = static::getValue('de');
		if($l === null)
			$l = static::setValue('de', new static('German'));
		return static::getValue('de');
	}
	
	/**
	 * The English language.
	 *
	 * @return \Language
	 */
	public static function EN() : \Language
	{
		$l = static::getValue('en');
		if($l === null)
			$l = static::setValue('en', new static('English'));
		return static::getValue('en');
	}
	
	/**
	 * The French language.
	 *
	 * @return \Language
	 */
	public static function FR() : \Language
	{
		$l = static::getValue('fr');
		if($l === null)
			$l = static::setValue('fr', new static('French'));
		return static::getValue('fr');
	}
	
	/**
	 * The name of the language, in english.
	 *
	 * @var string
	 */
	private $_name;
	
	/**
	 * Builds a new Language object with given name.
	 *
	 * @param string $name the name of the language, in english.
	 */
	private function __construct(string $name)
	{
		$this->_name = $name;
	}
	
	/**
	 * Gets the name of this Language object.
	 *
	 * @return string
	 */
	public function getName() : string
	{
		return $this->_name;
	}
	
}

```

This class declares three instances of the enumerable, which are `Language::DE()`,
`Language::EN()`, and `Language::FR()`. Those instances will have the
strings `'de'`, `'en'` and `'fr'` respectively set as their ids. 

Note also that `Language::EN() === Language::findById('en')` is
true, as the enumerable objects are designed to be singletons.


## License

MIT (See [license file](LICENSE)).
