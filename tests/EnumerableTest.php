<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-enumerable library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

use PHPUnit\Framework\TestCase;

/**
 * EnumerableTest test file.
 * 
 * @author Anastaszor
 * @covers \Enumerable
 *
 * @internal
 *
 * @small
 */
class EnumerableTest extends TestCase
{
	
	/**
	 * The object to test.
	 * 
	 * @var Enumerable
	 */
	protected Enumerable $_object;
	
	public function testToString() : void
	{
		$this->assertEquals('', $this->_object->__toString());
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PHPUnit\Framework\TestCase::setUp()
	 */
	protected function setUp() : void
	{
		$this->_object = $this->getMockForAbstractClass(Enumerable::class);
	}
	
}
