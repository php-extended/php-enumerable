<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-enumerable library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

/**
 * Enumerable class file.
 *
 * This class is the base of all Enumerable classes. This class considers
 * Enumerables as objects, and the way of calling them is by calling public
 * static methods that will be defined in the children class. This means that
 * those objects can be used into signature of methods as types hint for
 * checking parameters.
 *
 * Building those classes are similar as building enumerable objects into
 * languages like java, but with a little more code to add because of the lack
 * of built-in support from php.
 *
 * For example, if you want to make an enumerable about languages, the way to
 * do it is to define a subclass of Enumerable called Language, then a language
 * specific method like :
 *
 * <pre>
 * public static function EN() : \Language
 * {
 * 		$val = self::getValue('en');
 * 		if($val === null)
 * 			$val = self::setValue('en', new static('English'));
 * 		return $val;
 * }
 * </pre>
 *
 * This example is assuming that the constructor for this enumerable, which
 * should be private, accepts one arguments which is a string : the full name
 * of that language.
 *
 * This example should be later on called by Language::EN(). The object which
 * is returned is always the same, so if you have an instance of Language
 * enumerable, you can compare it with Language::EN() using the === operator.
 *
 * This example object can be stored into a database, or other persistant
 * storage (its toString method will return the 'en' key string), and can be
 * retrieved from the 'en' string with the Language::getById('en') method call.
 *
 * Enumerable classes you make should all be final, and should extend this
 * class directly, without multiple levels of inheritance. Constructors of the
 * children class should be made private.
 *
 * It is recommanded to use small strings (like 2 or 3 characters) for ids,
 * which will act like primary identifiers. You also may use integers, but
 * you should be careful that the given numbers of the identifiers should not
 * change when new versions of that Enumerable subclass are up. If that list
 * changes, all values that may be stored into persistant storages can
 * translate to a different value, or not being able to be translated into an
 * object again. Increasing versions and adding new enumerable objects should
 * be like getting a new number from an unique auto_increment sequence.
 *
 * @author Anastaszor
 * @SuppressWarnings("PHPMD.CamelCaseVariableName") // does not work with static variables
 * @SuppressWarnings("PHPMD.MissingImports")        // does not work in global namespace
 * @SuppressWarnings("PHPMD.UndefinedVariable")     // does not work with static variables
 */
abstract class Enumerable implements Stringable
{
	/**
	 * This will be true if all values are awaiken, i.e. if the values()
	 * method has been called.
	 *
	 * @var array<string, boolean>
	 */
	private static array $_awaiken = [];
	
	/**
	 * All the values of Enumerable objects. This object stores all the known
	 * Enumerable object for the current php run. Enumerable objects are stored
	 * into a 2 dimension array, the first dimension being the name of the
	 * subclass of Enumerable from which the enumerable objects are created.
	 * The second dimension being the id that is given to those enumerable
	 * objects.
	 * 
	 * For phpstan the type of this variable is static[][] but mutiple child
	 * classes may use it, but it is better described as \Enumerable[][]
	 *
	 * @var array<string, array<string, static>>
	 */
	private static array $_values = [];
	
	/**
	 * Get all values of this enumerable. This is made by calling all public
	 * static methods other than values() with no parameters. Those methods
	 * should initialize the $_values array from this class by adding their
	 * own component.
	 *
	 * Calling this method will call in cascade a lot of methods of the
	 * subclass, so you should take care that the children methods do not call
	 * the values() method (otherwise some stack overflow may happen).
	 *
	 * Calling this method from the Enumerable class will do nothing, ever if
	 * this class is abstract and cannot be instanciated, because it has no
	 * other public static methods with no parameters ; and will return an
	 * empty array.
	 *
	 * @return array<string, static>
	 * @throws ReflectionException
	 */
	public static function values() : array
	{
		$classname = static::class;
		if(!isset(Enumerable::$_awaiken[$classname]))
		{
			if(!isset(Enumerable::$_values[$classname]))
			{
				self::$_values[$classname] = [];
			}
			
			$class = new ReflectionClass($classname);
			$methods = $class->getMethods(ReflectionMethod::IS_STATIC);
			
			foreach($methods as $method)
			{
				/** @var ReflectionMethod $method */
				if('values' === $method->getName())
				{
					continue;
				}
				
				if($method->isPublic() && 0 === $method->getNumberOfParameters())
				{
					$method->invoke(null);
				}
			}
			
			self::$_awaiken[$classname] = true;
			\ksort(Enumerable::$_values[$classname]);
		}
		
		return self::$_values[$classname];
	}
	
	/**
	 * Gets the enumerable from given id. This method can be used as a getter
	 * of the enumerable object from any persistant storage like a database.
	 * If the object with the given id is not found for called class, then the
	 * returnDefault object (defaults to null) is returned.
	 *
	 * @param string $identifier the identifier of the object to search for
	 * @param ?static $returnDefault what should be returned when not found
	 * @return ?static the found object with given id
	 * @throws ReflectionException
	 */
	public static function getById(string $identifier, ?Enumerable $returnDefault = null) : ?Enumerable
	{
		$values = static::values();
		
		if(isset($values[$identifier]))
		{
			return $values[$identifier];
		}
		
		return $returnDefault;
	}
	
	/**
	 * Sets the given value to the given key.
	 * @param string $key
	 * @param static $value
	 * @return static the passed value
	 */
	protected static function setValue(string $key, Enumerable $value) : Enumerable
	{
		$classname = static::class;
		
		self::$_values[$classname][$key] = $value;
		$value->_id = $key;
		
		return $value;
	}
	
	/**
	 * Gets a given value from the given key.
	 * @param string $key
	 * @return ?static
	 */
	protected static function getValue(string $key) : ?Enumerable
	{
		$classname = static::class;
		
		if(isset(Enumerable::$_values[$classname][$key]))
		{
			return self::$_values[$classname][$key];
		}
		
		return null;
	}
	
	/**
	 * The string that represents the id of this enumerable. Enumerables from
	 * multiple subclasses may have the same id, although, enumerables from the
	 * same subclass must all have different ids.
	 *
	 * @var ?string
	 */
	protected ?string $_id = null;
	
	/**
	 * Magic getter method. This method will be called if a field call is
	 * performed over this object and cannot be resolved. This method transforms
	 * the name of the field by the equivalent getter method, and calls the
	 * getter method if it exists.
	 *
	 * If the getter does not exists, then this method throws an exception.
	 *
	 * @param string $name the property name to call for
	 * @return null|boolean|integer|float|string|object|array<integer|string, null|boolean|integer|float|string|object|array<integer|string, null|boolean|integer|float|string|object>> the result of the getter
	 * @throws Exception if the getter does not exists
	 * @psalm-suppress MixedInferredReturnType
	 */
	public function __get(string $name)
	{
		$getter = 'get'.\ucfirst($name);
		
		if(\method_exists($this, $getter))
		{
			/** @psalm-suppress MixedReturnStatement */
			return $this->{$getter}();
		}
		
		throw new Exception('The field '.$name.' does not exists in this enumerable object ('.static::class.' : '.$this->getId().')');
	}
	
	/**
	 * Magic setter method. This method will do nothing and throws an exception.
	 * 
	 * @param string $name
	 * @param null|boolean|integer|float|string|object|array<integer|string, null|boolean|integer|float|string|object|array<integer|string, null|boolean|integer|float|string|object>> $value
	 * @throws Exception
	 */
	public function __set(string $name, $value) : void
	{
		throw new Exception('No enum property can be set.');
	}
	
	/**
	 * Magic isset method. This method returns if a property exists.
	 * 
	 * @param string $name
	 * @return boolean
	 */
	public function __isset(string $name) : bool
	{
		return \property_exists($this, $name);
	}
	
	/**
	 * Magic unset method. This method does nothing and throws an exception.
	 * 
	 * @param string $name
	 * @throws Exception
	 */
	public function __unset(string $name) : void
	{
		throw new Exception('No enum property can be unset.');
	}
	
	/**
	 * Magic call method. This method does nothing and throws an exception.
	 * 
	 * @param string $name
	 * @param array<integer|string, null|boolean|integer|float|string|object|array<integer|string, null|boolean|integer|float|string|object>> $arguments
	 * @throws Exception
	 */
	public function __call(string $name, array $arguments) : void
	{
		throw new Exception('No enum method can be called.');
	}
	
	/**
	 * Magic call static method. This method does nothing and throws an exception.
	 * 
	 * @param string $name
	 * @param array<integer|string, null|boolean|integer|float|string|object|array<integer|string, null|boolean|integer|float|string|object>> $arguments
	 * @throws Exception
	 */
	public static function __callStatic(string $name, array $arguments) : void
	{
		throw new Exception('No enum static method can be called.');
	}
	
	/**
	 * Magic clone method. This method disallows any cloning.
	 * 
	 * @throws Exception
	 */
	public function __clone()
	{
		throw new Exception('No enum clones of objects are allowed.');
	}
	
	/**
	 * {@inheritDoc}
	 * @see \Stringable::__toString()
	 */
	public function __toString() : string
	{
		return (string) $this->getId();
	}
	
	/**
	 * Gets the id of this object. The id is the key that is used by the
	 * subclasses to register the enumerable objects when performing
	 * initialisation of this class.
	 *
	 * @return string
	 */
	public function getId() : string
	{
		return (string) $this->_id;
	}
	
	/**
	 * Gets whether this object is equal to another object. A Enumerable object
	 * is only equal to another object if the other object is an Enumerable of
	 * the same subclass and their ids are equal.
	 *
	 * @param null|boolean|integer|float|string|object|array<integer|string, null|boolean|integer|float|string|object|array<integer|string, null|boolean|integer|float|string|object>> $object
	 * @return boolean whether both objects are equals or not
	 */
	public function equals($object) : bool
	{
		// filters non objects
		if($object instanceof self)
		{
			// filters enumerable children classes
			if(\get_class($object) === static::class)
			{
				return $object->getId() === $this->getId();
			}
		}
		
		return false;
	}
	
}
